def changeRGB(picture):
  xMax = getWidth(picture)
  yMax = getHeight(picture)
  for col in range(0, xMax):
    for row in range(0, yMax):
      pixel = getPixelAt(picture, col, row)
      if(getBlue(pixel)):
        getBlue(pixel)
        setBlue(pixel, 255*2)
      if (getRed(pixel)):
        getRed(pixel)
        setRed(pixel, 255/2)
      if (getGreen(pixel)):
        getGreen(pixel)
        setGreen(pixel, 255/2)
        
        
  return picture
               
file =pickAFile()
myPic = makePicture(file)
changedPic = changeRGB(myPic)
show(changedPic)