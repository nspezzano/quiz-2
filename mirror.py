def mirrorHalf(picture):
  mirrorPoint = getHeight(picture)/2
  height = getHeight(picture)
  for x in range(0, getWidth(picture)):
    for y in range(0, mirrorPoint):
      topPixel = getPixel(picture, x, y)
      bottomPixel = getPixel(picture, x, height - y - 1)
      color = getColor(topPixel)
      setColor(bottomPixel, color)
    
file = pickAFile()
picture = makePicture(file) 
mirrorHalf(picture)
show(picture)