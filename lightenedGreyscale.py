def greyScale(pic):  
  for p in getPixels(picture):
    intensity = (getRed(p)+getGreen(p)+getBlue(p))/3
    setColor(p, makeColor(intensity+75, intensity+75, intensity+75))
    
file = pickAFile()
picture = makePicture(file) 
greyScale(picture)
show(picture)

